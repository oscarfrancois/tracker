# Projet H2-Lem

Projet de traceur GPS pour vélo à hydrogène.

## Introduction

Dans le cadre d'un projet de vélo à hydrogène nommé H2-Lem, on vous charge de développer la partie logicielle qui réceptionnera en continu les traces GPS envoyées via internet par le traceur GPS du vélo. Le logiciel devra aussi permettre d'afficher sur une carte le parcours instantané de chaque vélo ainsi que des indicateurs (ex: vitesse) et proposer une interface d'administration.

## Le principe des vélos à hydrogène

![hydrogen-bike-img](res/img/hydrogen-bike.jpg)

Les véhicules électriques actuels fonctionnent encore avec des batteries polluantes (exemple: les voitures Tesla à base de batteries lithium-ion, etc).
A contrario, l'hydrogène permet de stocker l'énergie sans polluer et sans problème de recyclage.
Grâce à ce qu'on appelle une pile à combustible (PAC), l'hydrogène est transformé en électricité via une réaction chimique et ne rejette que de l'eau!

![hydrogen-pac-img](res/img/pac.jpg)

![Vidéo de présentation](res/video/hydrogen-bike.webm)

## Cahier des charges

### Spécification fonctionnelle

Le service web de tracage GPS doit offrir les possibilités suivantes aux utilisateurs:

| ID  | Description                                                                                        |
|-----|----------------------------------------------------------------------------------------------------|
| SU1 | Le service web doit afficher une carte avec le tracé GPS du jour pour le véhicule sélectionné.     |
| SU2 | Le service web doit permettre de gérer plusieurs véhicules et d'afficher séparément leur tracé.    |
| SU3 | Le service web doit être capable de conserver un historique d'une année de trace GPS, soit environ |
|     | 6 millions de position lat,lon (estimation pour une flotte de 5 véhicules, 4H/jour, une trace/5s). |
| SU4 | Le service web doit permettre d'afficher la distance totale parcourue par jour, mois ou année      |
|     | glissante par véhicule ou pour la totalité de la flotte.                                           |
| SU5 | Le service web doit permettre d'afficher la vitesse actuelle d'un véhicule sélectionné.            |

### Spécification technique

Votre chef de projet a défini certains choix d'implémentation et certaines exigences techniques que vous devrez respecter.

#### Module de traceur GPS sur le vélo

Le module sera réalisé à base des composants suivants:

- 1 raspberry pi zéro avec micro-sdcard >1GB (cf. [raspberry](https://www.raspberrypi.org/products/raspberry-pi-zero) )
- 1 module GPS u-blox (cf. [gps-module](https://www.u-blox.com/en/product/neo-6-series) )
- 1 modulel SIM800 GSM (cf. [gsm-module](https://www.simcom.com/product/SIM800C.html) )
- 1 antenne SMA.
- 4 batteries 1.2V 1900mA.

Le tout sera fixé dans le fond du tube de vélo.

Cette partie sera réalisée par l'équipe logiciel embarqué.
S'il vous reste du temps après avoir fini votre partie, vous pourrez rejoindre cette équipe pour contribuer au logiciel embarqué.

#### Architecture technique

##### Système de cartographie ('map')

La solution [leaflet](https://leafletjs.com/) a été retenue pour l'affichage des tracés.

##### Cadre applicatif ('framework')

- le cadre applicatif ('framework') [bootstrap](https://getbootstrap.com/) a été retenu côté client ('front-end').

##### Méthodes de travail interne

- La méthode agile doit être utilisée.

- L'ensemble du code source doit être versioné avec l'outil git.

- Le développement dirigé par les tests (Test Driven Development, TDD) devra être privilégié quand cela est possible.

#### Langages de programmation

Les langages de programmation suivants peuvent être utilisés pour ce projet: PHP, python, nodejs.

#### Livrables

##### Documentation technique

Un manuel utilisateur ne vous est pas demandé. Cependant l'interface utilisateur doit être simple et intuitive pour une prise en main aisée. Des aides en ligne contextuelles (ex: icône d'information) seront ajoutées à certaines interfaces pour en expliquer le fonctionnement.

##### Code source

Le code source livré doit être documenté, respecter les bonnes pratiques logiciel (code structuré, propre et épuré, etc) et versioné avec git.

##### Jeux d'essai

Des jeux d'essai sous forme de tests unitaires et de procédures de tests manuels doivent être livrés. Ils doivent couvrir l'ensemble des exigences stipulées dans l'énoncé et aussi couvrir les cas raisonnablement probables d'usage incorrect du logiciel ou de défaillance.
