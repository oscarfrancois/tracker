# Configuration d'un serveur web natif Linux

![linux-tux](res/img/tux-320x200.png)

## Introduction

Une part très majoritaire des sites internet fonctionnent via des systèmes Linux natifs.
Nous présenterons ici les étapes de configuration en partant volontairement d'un système minimaliste à des fins éducatives.

Remarque: la plupart des commandes suivantes doivent être exécutées sous l'utilisateur `root` car elles touchent aux fonctions de base du système.

Pouur rappel, vous pouvez passer en tant qu'utilisateur `root` via la commande:

`su -`

## Services pré-installés

Le conteneur de base a déjà le service ssh d'activé.

Pour le vérifier:

`systemctl status ssh`

vous devez alors constater le statut: "active (running)"

## Mise à jour du système

Un réflexe à avoir est de commencer par mettre à jour l'ensemble des paquets logiciel du système.

Pour cela, il faut exécuter:

`apt update`

`apt upgrade -y`

## Ajout d'un utilisateur

Pour des raisons de sécurité, le service ssh interdit de base une connexion distante directemment en tant qu'utilisateur `root`.

Nous ajoutons donc le compte `admin` que nous utiliserons ensuite pour toutes les actions ne nécessitant pas de droits `root`.

`adduser admin`

## Installation du serveur web

Nous faisons le choix d'utiliser nginx comme serveur web car il est de plus en plus populaire (pour de bonnes raisons).

`apt install -y nginx`

Une fois installé, vous devriez via votre navigateur voir la page par défaut de nginx en vous rendant à l'URL/adresse IP de votre serveur.

## Affectation des droits à l'utilisateur admin

Nous allons changer les droits pour que l'utilisateur admin soit habilité à écrire dans les dossiers du serveur web.
Pour cela, nous commencerons par donner en recursif tous les droits à l'utilisateur et au groupe www-data via la commande:

`chown -R www-data:www-data /var/www`

Ensuite nous allons rajouter l'utilisateur admin au groupe `www-data` via la commande suivante:

`usermod -a -G www-data admin`

Attention: il faut ensuite relancer la session ouverte pour que ce changement soit pris en compte (ou sinon vous pouvez taper: `newgrp www-data` pour que le changement soit pris en compte sans vous déconnecter).

Les permissions de l'utilisateur `admin` seront celles applicable au groupe `www-data` pour chaque fichier.
De base, il n'y a que les droits de lecture. Nous allons donner aussi les droits d'écriture via la commande:

`chmod -R g+w /var`

## Activation de PHP

### Il faut tout d'abord installer le service php pour nginx

`apt  install -y php-fpm`

### Editez le fichier de configuration du serveur nginx pour activer le support de PHP:

`/etc/nginx/sites-available/default`

Faites les modifications pour avoir la section suivante activée exactement comme suit:

`location ~ \.php$ {`

`    include snippets/fastcgi-php.conf;`

`    fastcgi_pass unix:/run/php/php7.3-fpm.sock;`

`}`

Autre section à mettre à jour pour que les fichiers aux extension php soient prises en compte:

`index index.php index.html index.htm index.nginx-debian.html;`

Demandez au service web qu'il prenne en compte ce changement

`systemctl reload nginx`

Vérifiez que le PHP est bien actif en créant un fichier index.php à la racine du site web affichant les informations concernant l'environnement PHP installé:

`echo "<?php `phpinfo() ?>" > /var/www/html/index.php`

En pointant votre navigateur à la racine de votre site web, vous devriez voir ce type de page:

![phpinfo](res/img/phpinfo.png)

## Installation d'outils de téléchargement

Il est très pratique de pouvoir télécharger directement des ressources.
Pour cela, l'outil `curl `est la référence.

`apt install -y curl`

Exemple d'utilisation:

`curl https://leafletjs.com/examples/quick-start/example.html -o /var/www/html/index.html`

A noter que `curl` est aussi très utilisé en tant que librairie de téléchargement pour la plupart des langages de programmation.

## Accès à distance

Pour le développement à distance, on distinguera les phases de prototypage des phases de développement plus formel.

### Prototypage

Pour cette phase, on peut se contenter de monter le répertoire distant et de directement coder sans versioner son code.

### Sous linux / mac, on utilise par exemple la commande sshfs pour monter le répertoire distant via les commandes:

`sudo apt install -y sshfs`

`mkdir ~/mnt/remote`

`sshfs -o reconnect -o compression=yes admin@MON_SERVEUR_DISTANT:/var/www/html ~/mnt/remote`

Et pour déconnecter le lecteur:

`umount ~/mnt/remote`

### Sous windows, il faut installer les outils suivants pour activer le support d'sshfs

[sshfs-win](https://github.com/billziss-gh/sshfs-win)

[winfsp](https://github.com/billziss-gh/winfsp)

Et ensuite monter un lecteur réseau, lui assigner une lettre et renseigner l'URL comme suit:

`\\sshfs\admin@MON_SERVEUR_DISTANT\/`

Attention: n'oubliez pas les caractères `\/` à la fin de la ligne car c'est ce qui permet d'avoir accès à l'ensemble du système de fichier distant.

### Dépôt git

Cette phase plus formelle nécessitera la création d'un dépôt git dans lequel tous les changements seront versionnés.

Il n'est pas nécessaire de faire appel à des sites internet dédiés tels que gitlab ou github pour un versionage simple.
Vous pouvez tout simplement créer un dépôt git directement dans votre serveur via la commande:

`git init --bare /MON/CHEMIN/VERS/MON-PROJET.git`

Puis le cloner via l'URL: `ssh://admin@MON_SERVEUR_DISTANT/MON/CHEMIN/VERS/MON-PROJET.git`

## Surveillance des logs PHP

En cas de comportement anormal de votre service web, des informations précieuses seront probablement affichées dans les logs du serveur web.
Pour les afficher, ouvrez une session ssh distante et exécutez:

`tail -f /var/log/nginx/error.log`

## Sécurisation de votre serveur web

### Bonnes pratiques

Il convient de rappeler qu'une attention particulière doit être portée au choix de vos mots de passe et à leur renouvellement.

Il est aussi bon de rappeler que moins vous exposez vos services sur Internet, moins vous aurez besoin de passer du temps à les sécuriser.

### Restriction des accès

Lors de la phase de développement, il est préférable de limiter les accès aux seuls membres de l'équipe.

Il faut pour cela créer le fichier de mot de passe `/var/www/.htpasswd`

Ce fichier contient les noms d'utilisateur et les mots de passe cryptés de chaque utilisateur autorisé.

Il faut tout d'abord installer le paquet fournissant la commande `htpasswd` pour générer ces mots de passe:

`apt install -y apache2-utils`

Puis créer un utilisateur autorisé

`htpasswd -c /var/www/.htpasswd MON_NOM_D_UTILISATEUR_A_AUTORISER`

Ensuite, il faut activer cette sécurité dans nginx via les directives suivantes à insérer dans la section `serveur`:

`auth_basic "Veuillez vous identifier";`

`auth_basic_user_file /var/www/.htpasswd`;

Enfin, il faut demander à nginx de prendre en compte ce changement:

`systemctl reload nginx`

### Ne pas afficher la version du serveur web

De base les serveurs web fournissent leur version exacte aux navigateur. Cela vous expose à des attaques ciblées.

### Pour aller plus loin

La sécurité est un domaine complexe. Posez-vous d'abord la question: quels sont les problèmes qui peuvent raisonnablement arriver et quels en seront les conséquences. Les mesures de sécurité doivent seulement venir ensuite.


